export enum GameChoice {
  Rock = 'Rock',
  Paper = 'Paper',
  Scissors = 'Scissors'
}

export enum GameStage {
  NewGame,
  SetPlayers,
  Play,
  TurnResult
}

export class Turn {
  public p1: GameChoice | undefined;
  public p2: GameChoice | undefined;
  public draw: boolean = false;
  public winner: string = '';
}

export class GameService {
  // We could use an array of players instead of player1, player2...
  player1: string = '';
  player2: string = '';
  turnsToWin: number = 3;
  turns: Turn[] = [];
  isGameOver: boolean = false;
  winner: string = '';
  stage: GameStage = GameStage.NewGame;
  p1: GameChoice | undefined;
  p2: GameChoice | undefined;

  start() {
    // Initialize a new game (3 wins for the game)
    this._newGame(3);
    this._resetPlayers();
  }

  restart() {
    // Initialize a new game (3 wins for the game)
    this._newGame(3);
    this._newTurn();
  }

  setPlayers(player1: string, player2: string): void {
    if (player1 !== '' && player2 !== '') {
      this.player1 = player1;
      this.player2 = player2;
      this._newTurn();
    }
  }

  newTurn() {
    this._newTurn();
  }

  /**
   * handle user choice and check if both users have played
   *
   * @param player 1 for player #1, 2 for player #2
   * @param choice Rock, Paper or Scissors
   */
  choose(player: number, choice: GameChoice): void {
    if (player === 1) { this.p1 = choice; } else { this.p2 = choice; }
    // Both played ?
    if (this.p1 && this.p2) {
      this._playTurn(this.p1, this.p2);
      this.stage = GameStage.TurnResult;
    }
  }

  latestTurn(): Turn {
    if (this.turns.length === 0) { return {} as Turn; }
    return this.turns[this.turns.length - 1];
  }

  private _newGame(turnsToWin: number): void {
    this.turnsToWin = turnsToWin;
    this.turns = [];
    this.isGameOver = false;
    this.winner = '';
  }

  private _resetPlayers(): void {
    this.player1 = '';
    this.player2 = '';
    this.stage = GameStage.SetPlayers;
  }

  private _newTurn(): void {
    // Reset choices
    this.p1 = undefined;
    this.p2 = undefined;
    this.stage = GameStage.Play;
  }

  private _playTurn(p1: GameChoice, p2: GameChoice): void {
    if (!this.isGameOver) {
      const draw = p1 === p2;
      let winner = 'Nobody';
      if (!draw) {
        // If not draw, who wins this turn?
        winner = (
          (p1 === GameChoice.Paper && p2 === GameChoice.Rock) ||
          (p1 === GameChoice.Rock && p2 === GameChoice.Scissors) ||
          (p1 === GameChoice.Scissors && p2 === GameChoice.Paper))
          ? this.player1
          : this.player2;
      }
      const turn = { p1, p2, draw, winner };
      this.turns.push(turn);

      // Let's check if the game is over (if necessary)
      if (this.turns.length < this.turnsToWin) { return; }

      let countP1 = 0;
      let countP2 = 0;
      const instance = this;
      this.turns.forEach(t => {
        if (t.winner === instance.player1) {
          countP1++;
        } else if (t.winner === instance.player2) {
          countP2++;
        }
      });

      if (countP1 >= this.turnsToWin) {
        // P1 wins
        this.winner = this.player1;
        this.isGameOver = true;
      } else if (countP2 >= this.turnsToWin) {
        // P2 wins
        this.winner = this.player2;
        this.isGameOver = true;
      }
    }
  }
}
