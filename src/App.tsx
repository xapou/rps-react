import React from 'react';
import './App.css';
import { GameService, GameChoice, GameStage } from './game.service';

const App: React.FC = () => {
  const [state, setState] = React.useState({
    game: new GameService()
  });
  
  const newGame = () => {
    state.game.start();
    setState({game: state.game});
  }

  const restart = () => {
    state.game.restart();
    setState({game: state.game});
  }

  const setPlayers = (e: any) => {
    e.preventDefault();
    if (!state.game.player1.length || !state.game.player2.length) {
      return;
    }
    state.game.setPlayers(state.game.player1, state.game.player2);
    setState({game: state.game});
  }

  const player1Change = (e: any) => {
    state.game.player1 = e.target.value;
    setState({game: state.game});
  }

  const player2Change = (e: any) => {
    state.game.player2 = e.target.value;
    setState({game: state.game});
  }

  const choose = (player: number, choice: GameChoice) => {
    state.game.choose(player, choice);
    setState({game: state.game});
  }

  const newTurn = () => {
    state.game.newTurn();
    setState({game: state.game});
  }

  // Table content
  const tableRows = state.game.turns.map((turn, index) => 
    <tr key={index.toString()}>
      <th>#{index + 1}</th>
      <td>{turn.p1}</td>
      <td>{turn.p2}</td>
      <td>{turn.winner}</td>
    </tr>
  );

  return (
    <div className="App">
    { state.game.stage === GameStage.NewGame && 
      <div>
        <p>Hello ! Let's play Rock-Paper-Scissors...</p>
        <button onClick={newGame}>New Game</button>
      </div>
    }
    { state.game.stage === GameStage.SetPlayers && 
      <div>
        <p>Enter the names of both players...</p>
        <form onSubmit={setPlayers}>
          <input id="player1" placeholder="Player1" onChange={player1Change} value={state.game.player1} />
          <input id="player2" placeholder="Player2" onChange={player2Change} value={state.game.player2} />
          <button type="submit">Play</button>
        </form>
      </div>
    }
    { state.game.stage === GameStage.Play && 
      <div>
        <div>
          <h2>{state.game.player1} {state.game.p1 && <span role="img" aria-label="Thumbs up">👍🏻</span>}</h2>
          {!state.game.p1 && 
            <div>
              <button onClick={() => choose(1, GameChoice.Rock)}>Rock</button>
              <button onClick={() => choose(1, GameChoice.Paper)}>Paper</button>
              <button onClick={() => choose(1, GameChoice.Scissors)}>Scissors</button>
            </div>
          }
        </div>
        <div>
          <h2>{state.game.player2} {state.game.p2 && <span role="img" aria-label="Thumbs up">👍🏻</span>}</h2>
          {!state.game.p2 && 
            <div>
              <button onClick={() => choose(2, GameChoice.Rock)}>Rock</button>
              <button onClick={() => choose(2, GameChoice.Paper)}>Paper</button>
              <button onClick={() => choose(2, GameChoice.Scissors)}>Scissors</button>
            </div>
          }
        </div>
      </div>
    }
    { state.game.stage === GameStage.TurnResult && 
      <div>
        { !state.game.isGameOver && 
          <div>
            <h2>{state.game.latestTurn().winner} wins the turn !</h2>
            <button onClick={newTurn}>New turn</button>
          </div>
        }
        { state.game.isGameOver && 
          <div>
            <h2>{state.game.winner} wins the game !</h2>
            <table className="center">
              <thead>
                <tr>
                  <th>Turn</th>
                  <th>{state.game.player1}</th>
                  <th>{state.game.player2}</th>
                  <th>Winner</th>
                </tr>
              </thead>
              <tbody>
                {tableRows}
              </tbody>
            </table>
            <button onClick={newGame}>Reset</button>
            <button onClick={restart}>Restart</button>
          </div>
        }
      </div>
    }
    </div>
  );
}

export default App;
